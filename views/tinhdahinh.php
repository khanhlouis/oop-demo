<?php
    abstract class smartPhone
    {
        public $touch;
        public $pysical;
        public $camera;

        public function setData($touch = null, $pysical = null, $camera = null)
        {
            $this->touch      = $touch;
            $this->pysical    = $pysical;
            $this->camera     = $camera;
        }
    }
// nhiều lớp con có thể extends dữ liệu từ lớp cha mà không nhầm lẫn
    class phone extends smartPhone{
        
        public function __construct($screen, $button, $camera)
        {
            $this->setData($screen, $button,$camera);
        }

        public function outputData()
        {
            echo $this->touch;
            echo "<br>";
            echo $this->pysical;
            echo "<br>";
            echo $this->camera;
            echo "<br>";
        }


    }

    $iphone5s = new phone("4 inch", 1, "8 pixel");

    $samsung6 = new phone("5.1 inch", 1, "16 pixel");

    $iphone5s->outputData();
    echo "<hr>";
    $samsung6->outputData();
?>

