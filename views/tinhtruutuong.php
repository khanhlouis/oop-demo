<?php
    abstract class smartPhone{
        public $touch_screen;
        public $physical_button;
        public $camera;

        public function setData($touch_screen = null,$physical_button = null,$camera = null)
        {
            $this->touch_screen      = $touch_screen;
            $this->physical_button   = $physical_button;
            $this->camera            = $camera;
        }
    }

    class  iphone extends smartPhone{
        public function __construct($screen, $button, $camera)
        {
            $this->setData($screen, $button, $camera);
        }

        public function outputData()
        {
            echo  $this->touch_screen;
            echo "<br/>";
            echo $this->physical_button;
            echo "<br>";
            echo $this->camera;
        }
    }
$iphone5s = new iphone("4 inch", 1 , "8 megapitxel");
$iphone5s->outputData();
   ?>