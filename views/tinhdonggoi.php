<?php 

// Tất cả mọi thao tác truy xuất vào thành phần dữ liệu từ đối tượng
// này qua đối tượng khác phải được thực hiện bởi các phương thức (method) của chính đối tượng chứa dữ liệu.
abstract class smartPhone{
    
    public $touch_screen;
    public $physical_buuton;
    public $camera;
    protected $Os;
    private $account;

    public function setData($touch_screen = null, $physical_button = null, $camera = null,  $Os = null, $account = null)
    {
        $this->touch_screen     = $touch_screen;
        $this->physical_button  = $physical_button;
        $this->camera           = $camera;
        $this->Os               = $Os;
        $this->account          = $account;
    }

    public function setAccount($acc)
    {
        $this->account = $acc;
    }

    public function getAccount()
    {
        return $this->account;
    }
}

class iphone extends smartPhone{
    public function __construct($screen, $button, $camera, $Os)
    {
        $this->setData($screen, $button, $camera, $Os);
    }

    public function outputData()
    {
        echo $this->touch_screen;
        echo "<br>";
        echo $this->physical_button;
        echo "<br>";
        echo $this->camera;
        echo "<br>";
        echo $this->Os;
        echo "<br>";
    }
}

$iphoneX = new iphone("5.8 inch", 0 , "16 pixel", "12.0.1");
$iphoneX->outputData();

$iphoneX->setAccount('nguyen@gmail.com');

echo $iphoneX->getAccount();
?>