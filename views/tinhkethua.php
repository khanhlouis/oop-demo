<?php

    class smartPhone
    {
        public $touch;
        public $physical;
        public $camera;

        public function setData($touch = null, $physical = null, $camera = null)
        {
            $this->touch        = $touch;
            $this->physical     = $physical;
            $this->camera       = $camera;
        }

    }

// lớp iphone kế thừa tất cả thuộc tính trong lớp smartPhone và lớp iphone có thể thêm thuộc tính mới
class iphone extends smartPhone{
    public $touchID     = "YES";


    public function __construct($screen, $button, $camera){
        $this->setData($screen, $button, $camera);
    }
    public function outputData(){
        echo $this->touch;
        echo "<br />";
        echo $this->physical;
        echo "<br />";
        echo $this->camera;
        echo "<br />";
        echo $this->touchID;
    }
}

$iphoneX = new iphone("5.8 inch", 0 , "20 pixel");
$iphoneX->outputData();
?>