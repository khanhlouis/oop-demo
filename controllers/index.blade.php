<?php
    include "config.php";
    include "database.php";
?>

<?php
    $db = new database();
    $query = "SELECT * FROM tbl_name";
    $read = $db->select($query);

?>

<table class="tblone" border="1 solid red">
    <tr>
        <th width="200px">Name</th>
        <th width="200px">Email</th>
        <th width="200px">Skill</th>
        <th width="200px">Action</th>
    </tr>

    <?php if ($read) { ?>
    <?php while ($row = $read->fetch_assoc()) { ?>
    <tr>
        <td style="text-align: center"><?php echo $row['name']; ?></td>
        <td style="text-align: center"><?php echo $row['email']; ?></td>
        <td style="text-align: center"><?php echo $row['skill']; ?></td>
        <td style="text-align: center"><a href="update.php?id=<?php echo $row['id']; ?>">Edit</a></td>
    </tr>

    <?php } ?>

    <?php } else { ?>
        <p>data is not available !!</p>
    <?php }?>

</table>